//问题关键在于判断打印的起始及终止条件

//打印矩阵的一圈可分成四次打印：
//（1）打印此圈的最上面一行：只有在该圈的列数不为0时方可打印；
//（2）打印此圈的最右边一列：只有在该圈的行数大于等于2时，方可打印；
//（3）打印此圈的最下边一行：只有在该圈的行数大于等于2，并且列数大于等于2时，方可打印；
//（4）打印此圈的最左边一列：只有在该圈的行数大于等于3，并且列数大于等于2时，方可打印。


vector<int> printMatrix(vector<vector<int> > matrix) {

	if (matrix.empty())           //矩阵为空
		return matrix[0];

	vector<int> res;
	int rows = matrix.size();             //行数
	int cols = matrix[0].size();          //列数
	int x = 0, y = 0;
	while (x * 2<rows && y * 2<cols)      //循环打印终止条件，打印终点位于矩阵中心
	{
		//最上面的一行。cols-y-1为当前圈最右边一列的索引值，包括了最后一列的右上角元素
		for (int i = y; i <= cols - y - 1; i++)
			res.push_back(matrix[x][i]);
		//最右边的一列，rows-x-1为当前圈最后一行的索引值
		if (rows - x - 1>x)
		{
            //从下一行开始打印，直到最后一行，包括了最后一行的右下角元素
			for (int i = x + 1; i <= rows - x - 1; i++)
				res.push_back(matrix[i][cols - y - 1]);
		}
		//最下面一行
		if (rows - x - 1>x && cols - y - 1>y)
		{
            //从倒数第二列开始打印，直到第一列，包括了最左边一列的左下角元素
			for (int i = cols - y - 2; i >= y; i--)
				res.push_back(matrix[rows - x - 1][i]);
		}
		//最左边一列
		if (rows - x - 2>x && cols - y - 1>y)
		{
            //从倒数第二行开始打印，直到第二行
			for (int i = rows - x - 2; i >= x + 1; i--)
				res.push_back(matrix[i][y]);
		}
        //进入下一圈
		x++;                   
		y++;
	}
	return res;
}
