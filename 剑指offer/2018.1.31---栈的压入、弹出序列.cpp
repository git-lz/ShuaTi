//栈的压入、弹出序列

//存在两个数组，栈的压入顺序数组和出栈顺序数组

//借助一个栈，模拟出压栈入栈过程
//依次将压栈数组中的元素入栈，若栈顶元素等于出栈中的元素，则依次出栈；否则，继续压栈

bool IsPopVector(vector<int> pushV,vector<int> popV)
{
    if(pushV.size()==0)       //若数组为空，返回false
        return false;
    
    stack<int> res;          //定义一个栈，模拟入栈出栈过程

    //依次入栈pushV中的元素pushV[i],比较res栈顶元素是否等于popV[j],若相等，则出栈
    for(int i=0,j=0;i<pushV.size();++i)   
    {
        res.push(pushV[i]);

        while(!res.empty() && res.top()==popV[j] && j<popV.size())
        {
            res.pop();
            ++j;
        }
    }

    //若res栈为空，意味着栈中所有元素都已出栈，popV符合出栈顺序；否则，不符合
    return res.empty();
}