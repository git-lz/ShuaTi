//问题描述：
//HZ偶尔会拿些专业问题来忽悠那些非计算机专业的同学。今天测试组开完会后,他又发话了:在古老的一维模式识别中,
//常常需要计算连续子向量的最大和,当向量全为正数的时候,问题很好解决。
//但是,如果向量中包含负数,是否应该包含某个负数,并期望旁边的正数会弥补它呢？例如:{6,-3,-2,7,-15,1,2,2},
//连续子向量的最大和为8(从第0个开始,到第3个为止)。你会不//会被他忽悠住？(子向量的长度至少是1)

//解题思路：利用动态规划实现
//分别设置当前向量和最大值max和返回结果res，初值都设置为数组第一个元素值

//从第二个元素开始，依次遍历整个数组：
//(1)更新max：max=max+当前位置元素>当前位置元素？max+当前位置元素：当前位置元素;
//(2)更新返回结果res：res=res>max?res:max;


int FindGreatestSumOfSubArray(vector<int> array) {
    if(array.size()==1)
        return array[0];
    int res=array[0],max=array[0];

    for(int i=1;i<array.size();++i)
    {
        max=(max+array[i])>array[i]?(max+array[i]):max;
        res=res>max?res:max;
    }
    return res;
}