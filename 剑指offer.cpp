#include<iostream>
using namespace std;

#include<vector>
#include<stack>

//从后向前输出链表元素
struct ListNode {
	int val;
	struct ListNode *next;
	ListNode(int x) :
		val(x), next(NULL) {
	}
};
vector<int> printListFromTailToHead(ListNode* head) {
	vector<int> res;
	if (!head)
	{
		res = {};
		return res;
	}
	stack<int> ss;
	ListNode *p = head;
	while (p != NULL)
	{
		ss.push(p->val);
		p = p->next;
	}
	while (!ss.empty())
	{
		res.push_back(ss.top());       //在数组尾部添加元素
		ss.pop();
	}
	return res;
}

//二叉树重建
struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
TreeNode* reConstructBinaryTree(vector<int> pre, vector<int> vin) {
	if (pre.size() == 0)
		return NULL;
	//创建根节点，根节点为先序遍历的第一个树
	TreeNode *root=new TreeNode(pre[0]);

	vector<int> pre_left, pre_right, vin_left, vin_right;
	
	//寻找根节点在中序遍历中的位置
	int root_index=0;
	for (int i = 0; i < vin.size(); i++)
	{
		if (pre[0] == vin[i])
		{
			root_index = i;
			break;
		}
	}

	//对于中序遍历，左子树在根节点的左边，右子树在根节点的右边
	for (int i = 0; i < root_index; i++)
	{
		pre_left.push_back(pre[i + 1]);
		vin_left.push_back(vin[i]);
	}
	for (int i = root_index + 1; i < vin.size(); i++)
	{
		pre_right.push_back(pre[i]);
		vin_right.push_back(vin[i]);
	}

	root->left = reConstructBinaryTree(pre_left, vin_left);
	root->right = reConstructBinaryTree(pre_right, vin_right);

	return root;		
}

//用两个栈实现队列
class Solution
{
public:
	void push(int node) {
		while(!stack2.empty())
		{
			stack1.push(stack2.top());
			stack2.pop();
		}
		stack1.push(node);
	}

	int pop() {
		while (!stack1.empty())
		{
			stack2.push(stack1.top());
			stack1.pop();
		}
		if (!stack2.empty())
		{
			int res = stack2.top();
			stack2.pop();
			return res;
		}
		else
			return 0;
	}

private:
	stack<int> stack1;
	stack<int> stack2;
};

//数组的旋转---二分法
int minNumberInRotateArray(vector<int> rotateArray) {
	int n = rotateArray.size();
	if (n == 0)
		return 0;
	int low = 0, high = n - 1;
	while (low<high)
	{
		int mid = low+(high - low) / 2;
		if (rotateArray[mid] > rotateArray[high])
			low = mid + 1;
		else if (rotateArray[mid] < rotateArray[high])
			high = mid;
		else
			high = high - 1;
	}
	return rotateArray[low];
}

//斐波那契数列的第n项
int Fibonacci(int n) {
	if (n == 1)
		return 1;
	if (n == 2)
		return 1;
	int *a = new int[n];
	a[0] = 1;
	a[1] = 1;
	int i = 3;
	while (i<n)
	{
		a[i] = a[i - 1] + a[i - 2];
		i++;
	}
	return a[n - 1];
}
//动态规划解法
int Fibonacci1(int n) {
	int f = 0, g = 1;
	while (n--) {
		g += f;
		f = g - f;
	}
	return f;
}

//青蛙跳台阶
int jumpFloor(int number) {
	int g = 1, f = 2;
	while (--number)
	{
		f += g;
		g = f - g;
	}
	return g;
}
//变态的青蛙跳台阶
int jumpFloorII(int number) {
	if (number == 1)
		return 1;
	if (number == 2)
		return 2;
	if (number == 0)
		return 0;
	int i = 0,res=0;
	while (i < number)
	{
		res += jumpFloorII(i);
		i++;
	}
	return ++res;
}

//矩形覆盖
int rectCover(int number) {
	if (number <= 0)
		return 0;
	if (number == 1)
		return 1;
	if (number == 2)
		return 2;
	return rectCover(number - 1) + rectCover(number - 2);
}

//统计数的二进制中1的个数
int  NumberOf1(int n) {
	int count = 0;
	while (n != 0) {
		++count;
		n = n & (n - 1);
	}
	return count;
}

//计算某个双精度数的整数次方---2018.1.24
double Power(double base, int exponent) {
	double res = 1.0;
	int expo = abs(exponent);
	while (expo>0)
	{
		res *= base;
		expo--;
	}
	return (exponent >= 0) ? res : (1.0 / res);
}

//调整数组顺序使奇数位于偶数前面---2018.1.24
void reOrderArray(vector<int> &array) {
	int n = array.size();
	if (n == 0 || n == 1)
		return;
	stack<int> s,ss;
	for (int i = 0; i < n; i++)
	{
		if (array[i] % 2 != 0)   //为奇数
			s.push(array[i]);    //奇数栈
		else
			ss.push(array[i]);   //偶数栈
	}
	while (!ss.empty())
	{
		array[n - 1] = ss.top();
		ss.pop();
		n--;
	}
	while (!s.empty())
	{
		array[n - 1] = s.top();
		s.pop();
		n--;
	}
}

//输出链表倒数第k个节点---2018.1.24
struct ListNode {
	int val;
	struct ListNode *next;
	ListNode(int x) :
		val(x), next(NULL) {
	}
}; 
ListNode* FindKthToTail(ListNode* pListHead, unsigned int k) {
	ListNode *pre = pListHead, *last = pListHead;
	int count = 0;
	for (; pre != NULL; count++)
	{
		if (count >= k)
		{
			last = last->next;
		}
		pre = pre->next;
	}
	return count<k ? NULL : last;
}
//链表反转---2018.1.24
ListNode* reverseList(ListNode* pListHead)
{
	ListNode *pre = nullptr, *next = nullptr;
	while (pListHead != NULL)
	{
		next = pListHead->next;
		pListHead->next = pre;
		pre = pListHead;
		pListHead = next;
	}
	return pre;
}

//合并两个已排序链表---2018.1.25
ListNode* Merge(ListNode* pHead1, ListNode* pHead2)
{
	if (!pHead1 && !pHead2)
		return NULL;
	if (!pHead1)
		return pHead2;
	if (!pHead2)
		return pHead1;
	ListNode *p, *res;

	if (pHead1->val <= pHead2->val)
	{
		res = pHead1;
		pHead1 = pHead1->next;
	}
	else
	{
		res = pHead2;
		pHead2 = pHead2->next;
	}
	p = res;
	while (pHead1 && pHead2)
	{
		if (pHead1->val <= pHead2->val)
		{
			p->next = pHead1;
			pHead1 = pHead1->next;
			p = p->next;
		}
		else
		{
			p->next = pHead2;
			pHead2 = pHead2->next;
			p = p->next;
		}
	}
	if (!pHead1)
		p->next = pHead1;
	if (!pHead2)
		p->next = pHead2;
	return res;
}

//判断是否是树的子结构---2018.1.25
bool isSubtree(TreeNode* pRootA, TreeNode* pRootB) {
	if (pRootB == NULL) return true;
	if (pRootA == NULL) return false;
	if (pRootB->val == pRootA->val) {
		return isSubtree(pRootA->left, pRootB->left)
			&& isSubtree(pRootA->right, pRootB->right);
	}
	else return false;
}
bool HasSubtree(TreeNode* pRootA, TreeNode* pRootB)
{
	if (pRootA == NULL || pRootB == NULL) return false;
	return isSubtree(pRootA, pRootB) ||
		HasSubtree(pRootA->left, pRootB) ||
		HasSubtree(pRootA->right, pRootB);
}

//计算二叉树的镜像
void Mirror(TreeNode *pRoot) {
	TreeNode *temp;
	if (pRoot != NULL)
	{
		temp = pRoot->left;
		pRoot->left = pRoot->right;
		pRoot->right = temp;
		if (pRoot->left != NULL)
			Mirror(pRoot->left);
		if (pRoot->right != NULL)
			Mirror(pRoot->right);
	}
}



template <class T>
void OutVector(vector<T> a)
{
	int n = a.size()-1,i=0;
	while (i<=n)
	{
		cout << a[i] << "  ";
		i++;
	}
}

int main()
{
	vector<int> a = { 1,2,3,4,5 };
	reOrderArray(a);
	OutVector(a);
}
